---
title: How to keep your Promises in check.
---
## _Let's talk about why we need to handle errors in promises and the right way to do it_

### What's a Promise?
You might be wondering why I have capitalized _P_ in Promise. Well if you guessed that my English is bad, you are partially correct, but I just wanted to highlight that promise is a JavaScript object and hence the capitalization. A promise is a placeholder object for a function that is taking a little time to do stuff that it's been instructed to do. 

You can think of it as this real-world example - You called your friend to make plans for the weekend but at the moment she was not able to give you a concrete answer, so she promised to get back to you _soon_ with the answer. Now your friend is true to her words she will eventually let you know even if she has to cancel plans.

The JavaScript Promise is an object that has three phases - 
- pending: initial state, neither fulfilled nor rejected.
- fulfilled: meaning that the operation was completed successfully.
- rejected: meaning that the operation failed.

These three states help us code ahead while we wait for an asynchronous method to do whatever it is doing. A promise has methods like any other JavaScript object and they can be called so that we can properly control the flow of our program when eventually the promise gets resolved.

Here is how it is used with a function. The function may or may not return something.

        function iAmAsynchronousFunction(...parameters){
            return new Promise( (resolve, reject) => {
                //I will do some very important stuff 
                //and I will take my time
                if(iWasSucessfullInDoingMyWork){
                    resolve();
                    //resolve(data);
                }else{
                    reject(error);
                }
            });
        };

If the promise is fulfilled you pass on the return data if any by calling resolve() or if the promise is rejected you call reject() with an error.
You can call reject without error- it will work but it's bad practice and we'll talk about it in the next section.

The three important methods are 
- then() - gets called when a promise is fulfilled.
- catch() - gets called when a promise is rejected.
- finally() - gets called regardless of rejection or fulfillment after the promise is no longer pending.

Here is the code example -

    iAmAsynchronousFunction("DO YOUR WORK FUNCTION")
        .then( (data) => {
            console.log(data);
            // some other things with data
        })
        .catch( (error) => {
            console.error(error);
        })
        .finally( ()=>{
            //finally is optional
            //finally doesn't take any parameter
            //Does the final task
        });

As you can see in the above code the use of then, catch, and finally helps us in coding ahead with the flow of logic in mind. With proper use of these and returning promises, we can chain a lot of them to make sure that an operation is done if and only if a previous operation has been successfully completed.

### Errors in Promises
Promises are implicitly kept under a try block that's why calling reject is the same as throwing an error. and hence follow the same principle of error throwing. A thrown error is caught by the most recent _.catch_ which in turn can rethrow it if the situation requires it.

For instance, this code:

    new Promise( (resolve, reject) => {
        throw new Error("Oh No!");
    }).catch( (error) =>{
        console.error(error); // Error: Oh No!
    });

…Works exactly the same as this:

    new Promise( (resolve, reject) => {
        reject(new Error("Oh No!"));
    }).catch( (error) =>{
        console.error(error); // Error: Oh No!
    }); 
    
I would suggest using reject in cases where we receive errors from a function and throw new Errors for custom errors to create and keep a distinction between them.

You might be wondering why we need to throw or handle errors while using a promise. Well because Promises are there to solve the problem in code readability that occurs when we encounter a callback hell but they are there to also provide a good flow of logic. Now you can't talk about the good flow of logic when your code does not even consider that the very important part the iAmAsynchronousFunction() which is taking its time might fail with an error. You do not want your whole program to stop or fall apart when part of it fails.

The requirement may arise when you want to do certain other things i.e call other functions (asynchronous or not), but that can be easily handled with proper and multiple placements of .catch(). See ..then and .catch both returns a promise so we can chain them and enjoy the benefits of conditional execution of code based on if a previous asynchronous code got executed properly or not.

Consider this code -

    // the execution: catch -> then
    new Promise( (resolve, reject) => {
        throw new Error("Oh No")
    })
    .catch( (error) =>{
        console.log("The error is handled, continue normally");
    })
    .then(() => {
        console.log("Next successful handler runs");
    });

the execution goes from catch because an error is thrown and then back to the subsequent then because .catch returns a promise and can be chained. This can be useful in a scenario when you have a web page like Facebook with a feed in the center, settings in the left, and chats/ people online in right. Imagine for some reason data from chat could not be fetched you don't want your whole page to stand still and display an error, so we can have a catch to throw an error about the chat not working, show a message like connection issue and go on get data for the feed and render it.

Consider the following code -

    // the execution: catch -> catch
    new Promise( (resolve, reject) => {
        throw new Error(PageBreakingErrorCode);
    })
    .catch( (error) =>{
        if(error == PageBreakingErrorCode ){
            throw PageBreakingErrorCode;
        }
        console.log("The error is handled, continue normally");
    })
    .then(() => {
        console.log("This will not be executed");
    })
    .catch( (error) =>{
        console.error(error);
    });
Here the error is checked if it's of a certain type and thrown to the next catch block and left to that _.catch_ to be handled.

A thoughtful and thorough combination of _.then_  _.catch_ chaining can be used to have solid error handling in one's program.
    